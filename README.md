## CFG Code Coverage

This tool has built for automatation code coverage with diffrent method's.
This code report the output of [website](https://cs.gmu.edu:8443/offutt/coverage/GraphCoverage) for each option. The Website builta according to Paul Ammann, Jeff Offutt software testing book.
In this project we use python version.3 and selenium framework.
This tool work for linux and windows platform.

## Requirement

- python3
- Firefox web browser
 
## Installation
For install the required packege:

```bash
virtualenv venv
source ./venv/bin/activate  # for linux
.\venv\Scripts\activate   # for windows
pip install -r requirements.txt
```

## Usage

First install firefox and add this package to your directory. 
For using this package in your code

Input of this class is dot file that show cfg and first and last node and return diffrent parameters.

```python
from code_coverage import cfg_coverage 
    
    sample = cfg_coverage('your file.dot', first node, last node)
    sample.graph_node()
    sample.graph_edge()
    sample.node_coverage()
    sample.edge_coverage()
    sample.simple_path()
    sample.prime_path()
    sample.prime_path_coverage()
    sample.close()
```
**you can see example in [here](https://gitlab.com/roshan_roshan/CFG_COVERAGE/-/blob/master/example.py)**

## License
[MIT](https://gitlab.com/roshan_roshan/CFG_COVERAGE/-/blob/master/LICENSE)
