import os
import platform
import ast
import networkx as nx
from selenium import webdriver
from selenium.webdriver.firefox.options import Options


def to_list(string_list):
    result = list()
    for i, j in enumerate(string_list):
        aa = ast.literal_eval(string_list[i])
        result.append(aa)
    return result


class cfg_coverage:

    """
    This class has built for automatation code coverage with diffrent method.
    This code report the output of https://cs.gmu.edu:8443/offutt/
    coverage/GraphCoverage for each option.
    We use selenium library for bulding this tools.

    ** For use this tool just need to input .dot file fullpath and call
     any function you

    for call this class and build new object of this class you nedd something
    like below:

        obj = path_coverage('graph.dot', first node number, last node number)
        obj.simple_path()

    The list of function:
        - graph_node()
        - graph_edge()
        - node_coverage()
        - edge_coverage()
        - simple_path()
        - prime_path()
        - prime_path_coverage()
    """

    abs_path = os.path.dirname(__file__)
    if platform.system() == "Linux":
        path = abs_path + "/gecko/geckodriver"
    elif platform.system() == "Windows":
        path = abs_path + "\\gecko\\geckodriver.exe"

    def __init__(self, file_path_name, first_node, last_node):

        self.graph = nx.drawing.nx_pydot.read_dot(file_path_name)
        self.alll = list()
        self.node_coverages = list()
        self.edge_coverages = list()
        self.simple_pathes = list()
        self.prime_pathes = list()
        self.paths = list()
        self.req = list()
        first = str(first_node)
        last = str(last_node)
        edges = self.read_graph_edges()
        opts = Options()
        opts.set_headless()
        self.driver = webdriver.Firefox(executable_path=cfg_coverage.path,
                                        options=opts)
        self.driver.get(
            "https://cs.gmu.edu:8443/offutt/coverage/GraphCoverage")
        self.driver.find_element_by_xpath(
            "/html/body/form/table[1]/tbody/tr/td[1]/table/ \
             tbody/tr[2]/td/textarea").send_keys(edges)
        self.driver.find_element_by_xpath(
            "/html/body/form/table[1]/tbody/tr/td[2]/table/ \
             tbody/tr[2]/td/input").send_keys(first)
        self.driver.find_element_by_xpath(
            "/html/body/form/table[1]/tbody/tr/td[3]/ \
             table/tbody/tr[2]/td/input").send_keys(last)

    def graph_node(self):
        # This function return all nodes in your cfg
        return self.graph.nodes()

    def graph_edge(self):
        # This function return all edges in your cfg
        return self.graph.edges()

    def read_graph_edges(self):
        """ This function change format of all edges in your cfg to
            specific format of website """
        edgess = str()
        for edg in self.graph.edges():
            edgess += str(edg[0]) + " " + edg[1] + "\n"
        return edgess

    def node_coverage(self):
        # This function return all pathes need for covering all nodes in cfg
        self.driver.find_element_by_xpath(
            '/html/body/form/table[2]/tbody/tr[8]/td[2]/input[1]').click()
        mytable = self.driver.find_element_by_xpath(
            '/html/body/form/table[3]/tbody/tr/td[1]')
        table_text = mytable.text
        for num, line in enumerate(table_text.splitlines()):
            if num == 0 and line == "":
                continue
            elif num != 0 and line == "":
                break
            else:
                self.node_coverages.append(line)
        self.node_coverages = to_list(self.node_coverages[1:])
        return self.node_coverages

    def edge_coverage(self):
        # This function return all pathes need for covering all edges in cfg
        self.driver.find_element_by_xpath(
            '/html/body/form/table[2]/tbody/tr[8]/td[2]/input[2]').click()
        mytable = self.driver.find_element_by_xpath(
            '/html/body/form/table[3]/tbody/tr/td[1]')
        table_text = mytable.text
        for num, line in enumerate(table_text.splitlines()):
            if num == 0 and line == "":
                continue
            elif num != 0 and line == "":
                break
            else:
                self.edge_coverages.append(line)
        print(self.edge_coverages[1:])
        self.edge_coverages = to_list(self.edge_coverages[1:])
        return self.edge_coverages

    def simple_path(self):
        """ This function return all simple pathes(path which does not has any
            cycle or tour) in cfg """
        self.driver.find_element_by_xpath(
            '/html/body/form/table[2]/tbody/tr[5]/td[2]/input[4]').click()
        mytable = self.driver.find_element_by_xpath(
            '/html/body/form/table[3]/tbody/tr/td[1]')
        table_text = mytable.text
        for num, line in enumerate(table_text.splitlines()):
            if num == 0 and line == "":
                continue
            elif num != 0 and line == "":
                break
            else:
                self.simple_pathes.append(line)
        self.simple_pathes = to_list(self.simple_pathes[1:])
        return self.simple_pathes

    def prime_path(self):
        # This function return all prime path in cfg
        self.driver.find_element_by_xpath(
            '/html/body/form/table[2]/tbody/tr[5]/td[2]/input[5]').click()
        mytable = self.driver.find_element_by_xpath(
            '/html/body/form/table[3]/tbody/tr/td[1]')
        table_text = mytable.text
        for num, line in enumerate(table_text.splitlines()):
            if num == 0 and line == "":
                continue
            elif num != 0 and line == "":
                break
            else:
                self.prime_pathes.append(line[3:])
        self.prime_pathes = to_list(self.prime_pathes[1:])
        return self.prime_pathes

    def prime_path_coverage(self):
        """ This function return all execution path need to covering all prime
          pathes in cfg
          ------------------
          return two set of path : first set return minimum set of path which
          if they are covered all of second set of path covering automatically
          """
        self.driver.find_element_by_xpath(
            '/html/body/form/table[2]/tbody/tr[9]/td[2]/input[3]').click()
        mytable = self.driver.find_element_by_xpath(
            '/html/body/form/table[3]/tbody/tr/td[1]/table[1]/tbody')
        for row in mytable.find_elements_by_css_selector('tr'):
            for cell in row.find_elements_by_tag_name('td'):
                self.alll.append(cell.text)

        for i, j in enumerate(self.alll):
            if (i + 1) % 2 == 0:
                self.req.append(j)
            else:
                self.paths.append(j)
        self.paths = to_list(self.paths[1:])
        self.req = to_list(self.req[1:])
        return self.paths, self.req

    def close(self):
        self.driver.quit()
